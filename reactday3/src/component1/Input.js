import React from "react";
import '../App.css';



class Input extends React.Component {
  constructor(prors) {
    super(prors);
    this.state = {
      inputname: " ",
      inputweigth: " "
    };
  }

  render() {
    return (
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-4">
            <input
              class="form-control"
              type="text"
              placeholder="name"
              onChange={e =>
                this.setState({
                  inputname: e.target.value
                })
              }
              value={this.state.inputname}
            />
          </div>
          <div class="col-4">
            <input
              class="form-control"
              type="text"
              placeholder="weigth"
              onChange={e =>
                this.setState({
                  inputweigth: e.target.value
                })
              }
              value={this.state.inputweigth}
            />
          </div>
          <div class="col-4">
            <input type="submit" class="btn btn-secondary" value="submit" onClick={() =>this.props.onClickAdd(this.state)} />
          </div>
        </div>
      </div>
    );
  }
}

export default Input;
